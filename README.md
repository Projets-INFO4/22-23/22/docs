# Projet Agriculture Connectée INFO4 22-23
> ABARNOU Eloïse, CAUCHY Marine, LEITE Jamile, PUECH Lilian

## Introduction au projet

Avant tout, voici quelques liens utiles :

* Groupe Dépôt Git : [lien](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/22-23/22)
* Serveur LoRA de Polytech (*pour les chipsets*) : [lien](https://lns.campusiot.imag.fr/#/organizations/6/applications/58)
* La liste des projets : [lien](https://air.imag.fr/index.php/Projets_2022-2023)
* Une introduction sur la NUCLEO-L073RZ : [lien](https://os.mbed.com/platforms/ST-Nucleo-L073RZ/)

Un peu de documentation se trouve également dans le dépôt git du code source.

### Démarrage du projet

Pour programmer la carte STM32, nous utilisons l'IDE **STM32CudeIDE**, sa version **1.11.0** que nous avons installé sur nos machines via le debian disponible sur le site de STMElectronics.

Pour ouvrir le projet, il est recommandé d'une part d'ouvrir Visual Code dans le répertoire courant du projet **docs/**, d'autre part de lancer l'IDE sur le workspace **workspace-1.11.0/** et ensuite, une fois l'IDE prêt, d'ouvrir le projet **watermonitoring** en allant dans **File** > **"Open Projects from File System..."** et d'écrire à **Import Source** le dossier parent de **watermonitoring/**. Une fois le projet ouvert, vous pouvez tester sa compilation avec le raccourci **[Ctrl]**+**[B]**.

### Contexte du projet

Une serre aquaponique à Polytech utilise des capteurs connectés à des cartes **STM32** elle-mêmes connectées à des radios **LoRA** afin d'envoyer des données au serveur LoRA de Polytech sur les conditions d'environnement (température, pH, humidité, ...) de la serre. Cette serre sert de plateforme de test à d'autres projets grandeur nature tel que le projet des jardins du Coteau (*lien git :* [lien](https://gricad-gitlab.univ-grenoble-alpes.fr/ASAC/serres-jdc/Code)).

La serre a été mise à l'arrêt (retrait de l'eau, du persil, etc) lors de l'arrivée du coronavirus en France, courant mars 2020, et elle n'a pas été remise en place depuis. Le but du projet est donc de reprendre le code d'antan des cartes STM32 de la serre, qui servaient à envoyer ses données d'environnement au serveur LoRA, afin d'accompagner la remise en fonction de celle-ci. Les langages de programmation utilisés sont les suivants:

* **C++** : Presque tout le temps utilisé pour coder les cartes
* **C** : Beaucoup plus rare pour coder les cartes
* **ARM** : Pour quelques détails des cartes STM32
* **JavaScript** : Programmer le Payload Codec sur le serveur LoRA (*le sujet est traîté plus bas*)

## Séances du 16 et 23 janvier

Durant les deux premières séances de projet, nous n'avons fait que de la mise à niveau et de la prise de conscience sur les enjeux du projet :

* Prise en main de STM32CudeIDE
* Découverte des cartes utilisées
* Découverte de la serre de Polytech
* Relecture du code et déboggage

Ce dernier point représente la tâche qui a été la plus longue à achever: nous n'avons découvert qu'après une semaine qu'il suffisait de retirer la ligne 31 du fichier source **FlashIAP.h** de **mbed/drivers/** afin d'être débarassés des 31 erreurs générées par le code d'antan. Ces 31 erreurs provenaient toutes de la librairie **stl_algobase.h** de **libstdc++14** et nous n'avons pas résolu ces erreurs, mais nous les avons plutôt évitées en retirant la ligne **"#include \<algorithms\>"** du fichier **FlashIAP.h**. Ces erreurs referont donc surface si nous avons un jour besoin de cette librairie, ce qui est très peu probable...

### Quelques informations utiles pour la suite

* La partie intéressante du code (***app/main.cpp***) se situe dans les commentaires et dans la fonction **PrepareTXFrame** qui remplit la structure de données LoRAWAN des données d'environnement de la serre (temp, pH, etc) des capteurs (*source : **capteur/capteur.cpp***).
* Le débit de la bande passante générale du réseau LoRA de Polytech est très faible, de plus que le serveur LoRA n'accorde qu'1% maximum de ce débit maximum à chaque appareil connecté afin d'éviter toute surcharge du réseau, il faudra donc en tenir compte dans notre code.
* Il n'y a pas de couche "Ethernet", "IP" puis "TCP", il y a la couche "LoRA", puis la couche "LoRAWAN", puis par dessus la couche d'encodage "Cayenne LPP" que nous n'utiliserons pas pour que le code soit moins verbeux. À la place, nous coderons plus tard, une fois le programme fonctionnel, notre propre encodage en JavaScript quand nous maîtriserons les données que nous gérons. Cependant, cette feature n'est pas centrale dans le projet et risque donc de ne pas être traîtée.

## Séance du 6 février

* Debug du code du projet afin de comprendre l’aspect technique de la technologie de communication à longue portée par radio à faible puissance d’émission LoRo. En effet, la prise en main de cette technologie d’étalement de spectre se passe par un debug du code, ainsi nous comprenons comment on se déplace dans les différents états du système.

* Enregistrement de nos cartes LoRa sur le serveur LoRa de l’école.

* Mettre les keys dans le cache pour pouvoir envoyer aux IESEs pour qu’ils puissent faire leurs mesures de consommation d’énergie.

## Séance du 20 février

Nous avons passé la séance à chercher à régler un problème de communication de cartes entre celle d'Eloise et le serveur LoRA de Polytech. Aucune issue n'a été trouvée à la fin de la séance.

## Séance du 27 février

Nous avons eu une séance de clarification sur les objectifs du projet.

### La serre

Dans la serre, il y a un boîtier muni d'une carte STM32 sur laquelle est branchée une carte fille qui réceptionne les données des capteurs et par-dessus une carte LoRA qui envoie les données au serveur LoRA.

### Le boîtier

Il contient également un écran à LED de 84 pixels par 48 pixels sur lequel les données acquises par les capteurs seront affichées à chaque nouvelle mesure. Le boîtier sera à l'avenir connecté à une autre carte STM32 qui s'occupera de mesurer les intensités de courant de la carte de puissance.

### L'objectif de l'intégration

Les anciens IESE4 qui se sont plongés sur notre projet ont terminé le code de la carte STM32. Ce code consiste en récupérer les valeurs de température, pH, oxgène et humidité acquises par les capteurs et à les inscrire dans une trame LoRA avant de l'envoyer sur le serveur :

```C++
uint8_t PH = stn.PH_uint(capt.return_pH());
int16_t Temp = stn.Temp_int(capt.return_temp());
uint16_t OXY  = stn.Oxy_int(capt.return_DO());
uint16_t COND = stn.Cond_int(capt.return_EC());

if( IsTxConfirmed == true ) {         
    AppData[0] = (uint8_t) 0x1C;
    AppData[1] = (uint8_t) (Temp>>8);
    AppData[2] = (uint8_t) Temp;
    AppData[3] = (uint8_t) 0x1D;
    AppData[4] = (uint8_t) PH;
    AppData[5] = (uint8_t) 0x1E;
    AppData[6] = (uint8_t) (OXY>>8);
    AppData[7] = (uint8_t) OXY;
    AppData[8] = (uint8_t) 0x1F;
    AppData[9] = (uint8_t) (COND>>8);
    AppData[10]= (uint8_t) COND; 
}
```

Maintenant, notre objectif est d'utiliser ces données acquises à chaque nouvelle mesure pour les afficher sur l'écran LCD du boîtier. Nous nous sommes déjà un peu exercés à afficher du texte sur un tel écran (même référence d'écran) via une carte Arduino.

Une fois la carte de puissance terminée et délivrée par le groupe de travail en IESE, nous nous occuperons de remettre en place le système de la serre en marche, c'est-à-dire la mise en place du boîtier et des capteurs connectés au courant. Cette carte de puissance est nécessaire quant à la redistribution du courant continu en entrée dans tous les capteurs, les vannes, le moteur et la conversion $24V \rightarrow 5V$ pour le boîtier.

### Préparation de la soutenance

Nous avons également consacré cette séance à la préparation de notre soutenance. Nous avons créé une présentation de 3 diapositives avec un diagramme de flux de tâches par quartile de temps, et une description de chaque tâche.

## Séance du 6 mars

### Avenant de la soutenance

Nous devons nous attenir à écrire du code, selon les obligations des modalités du projet, cependant, ce projet ne nous amène pas à coder beaucoup de lignes par nous-même. Néanmoins nous avons deux objectifs principaux qui sont susceptibles de mobiliser des nouvelles lignes de code: l'écran et le payload codec. Nous avons déjà discuté de ces features plus haut.

Organisation :

* **Codec** : par Jamile et Lilian
* **Écran** : par Marine et Éloïse

### Supplément sucre

Il est envisageable de faire en sorte que la **user led** (*PA5, GPIO13*) clignote quand le programme tourne sur la carte STM32. Ici, une couche applicative (dans ``hal.h``) nous permet d'écrire moins de lignes de code en ne modifiant pas directement les registres nécessaires à la main. Néanmoins il est quand même nécessaire de bien comprendre comment fonctionne la **user led** et quels branchements la lie à la carte STM32.

### Premier Payload Codec

Nous avons écrit un premier payload codec en JavaScript:

```JavaScript
// Some aux functions
function readUInt8BE(buf, offset) {
  return buf[offset]
}

function readUInt16BE (buf, offset) {
  return (buf[offset] << 8) | buf[offset + 1]
}

function readUInt32BE (buf, offset) {
  return (buf[offset] * 0x1000000) +
    ((buf[offset + 1] << 16) |
    (buf[offset + 2] << 8) |
    buf[offset + 3])
}

// Main function called to decode frames
function Decode(fPort, bytes) {
  var value = {}

  value.typeTest = readUInt8BE(bytes, 0)
  value.tempTest = readUInt16BE(bytes, 1)
  
  value.portTest = fPort

  return value
}
```

Lorsque le serveur applique le décodeur en JavaScript sur chaque frame capturée, il appelle la fonction principale de décodage ``function Decode(a,b)`` où ``a`` représente le port en flottant et ``bytes`` est le tableau d'octets bruts envoyés par chaque gateway.

Ici, nous n'envoyons que les données de température. La température a un préfixe qu'il faut également récupérer et dont il faudra bien vérifier la valeur (``0x1C`` pour la température) lorsqu'on développera un payload codec complet et déployable.

Nous avons également modifier légèrement le code embarqué de la fonction qui prépare les frames de données dans ``main.cpp``:

```c++
#ifdef DEBUG_TEMP
            uint8_t PH = 7;
            int16_t Temp = 25;
            uint16_t OXY = 6;
            uint16_t COND = 1;
#else
            uint8_t PH = stn.PH_uint(capt.return_pH());
            int16_t Temp = stn.Temp_int(capt.return_temp());
            uint16_t OXY  = stn.Oxy_int(capt.return_DO());
            uint16_t COND = stn.Cond_int(capt.return_EC());
#endif // DEBUG_TEMP
```

Les capteurs cherchés par notre programme sont ceux du boîtiers, ceux qui font réellement servir lors du déploiement de la serre. Ainsi, tant qu'on reste sur nos cartes personnelles, il faut mettre des valeurs arbitraires pour les données d'environnement de la serre. Nous automatisons cela via une ``#define`` appelée ``DEBUG_TEMP``.

### Premiers dessins à l'écran

Nous nous sommes concentrés sur le design attendu sur l'écran pour pouvoir commencer le code sur STM32 IDE.
L'objectif est de pouvoir faire une goute, pH, etc.

Le dessin pixel par pixel est un succès:

![Voir pdf](ecran.png "Figure 1")

## Séance du 13 mars

### Le codec final en JavaScript

Nous proposons la fonction suivante d'encodage du Payload Codec qui fonctionne (*testé avec nos cartes*):

```js
// Some aux functions
// Assuming Big Endian
function readUInt8BE(buf, offset) {
  return buf[offset];
}

function readUInt16BE (buf, offset) {
  return (buf[offset] << 8) | buf[offset + 1];
}

function readInt8BE (buf, offset) {
	if (buf[offset] > 127)
      return buf[offset] - 256;
  	return buf[offset];
}

function readInt16BE (buf, offset) {
  	var big = buf[offset] << 8;
  	if (big > 32767)
      big -= 65536;
  	return big + buf[offset+1];
}

// Main function called to decode frames
function Decode(fPort, bytes) {
  	/**
    Global data types
    */
  	var DATA_TEMPERATURE = 0x1C;
  	var DATA_PH = 0x1D;
 	var DATA_OXY = 0x1E;
  	var DATA_COND = 0x1F;
  
  	/**
    Global parser
    */
    var value = {};

    var index = 0;
    var continuer = 1;
    
    while (continuer) {
    	switch (readUInt8BE(bytes, index++)) {
          case DATA_TEMPERATURE:
            value.tempTest = readInt16BE(bytes, index);
            index += 2;
            break;
          case DATA_PH:
            value.pHTest = readUInt8BE(bytes, index);
            index += 1;
            break;
          case DATA_COND:
            value.condTest = readUInt16BE(bytes, index);
            index += 2;
            break;
          case DATA_OXY:
            value.oxyTest = readUInt16BE(bytes, index);
            index += 2;
            break;
          default:
            continuer = 0;
            break;
        }
    }

    return value
}
```

Nous n'avons laissé que les fonctions pour lire des entiers (signés ou non signés en big endian) de 8 bits et 16 bits car les valeurs mesurées ne varieront pas beaucoup (*par exemple, la température sera toujours entre -20°C et 50°C*).

### Et après le codec sur ChirpStack ?

**On approche du but final !**

La dernière étape du projet est son intégration fullstack via **Grafana**, **InfluxDB** et **Télégraf**. Avant l'arrêt de la serre de Polytech, c'était le service **NodeRed** qui était inscrit au serveur LoRA de Polytech pour récupérer les trames de données envoyées et les enregistrer dans la base de données **InfluxDB**. Cependant, les mises à jour sur **NodeRed** sont très fastidieuses et les IESE ont choisi de faire un portage de **NodeRed** à **Telegraf** pour l'enregistrement des données dans **InfluxDB** pour les serres du jardin des Coteaux. Il serait idéal d'effectuer ce même portage pour notre serre.

**Grafana** est une interface graphique web (*frontend*) pour l'affichage des données en temps réel:

![Voir pdf](grafana_off.png "Figure 2")

Entouré en vert, les données de notre serre qui seront affichées. Entouré en rouge, les données de la carte LAIRD (*où se situe-t-elle?? dans quelle serre est-elle installée??*).

Dans la base de données gérée par **InfluxDB**, chaque enregistrement de données est liée à une date (*un entier sur 64 bits qui représente le nombre de secondes depuis le 'epoch', le 1er Janvier 1970*). Grâce à cela, **Grafana** peut tracer les valeurs en fonction du temps, et calculer toutes sortes de données complexes en temps réel, comme calculer des régressions linéaires sur les courbes de température en fonction du temps pour déterminer la stabilité du système physico-chimique de la serre, etc.

### Le portage

Le lien git du projet FullStack NodeRed + InfluxDB + Grafana qu'il faut porter de NodeRed à Telegraf:

* Git : [lien](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/22-23/22/fullstack.git)
* Aperçu du FrontEnd : [lien](https://polytech.iot.imag.fr/d/jhNW0WRZk/serre-connectee-polytech?orgId=1)

Une fois le git importé, on exécute les commandes bash suivantes:
```bash
cd fullstack
docker compose up
docker exec -it fullstack_influxdb_1 influx v1 shell
create database serre
```

Pour sortir proprement de la base de données, il faut faire **[Ctrl]**+**[X]** puis **[Ctrl]**+**[C]**.

### Gestion de l'écran avec une carte STM32

Durant cette séance, nous avons commencé à adapter notre code d'affichage effectué en Arduino avec STM32CubeIDE. 
<<<<<<< HEAD
Pour cela nous avons commencé par créer un nouveau projet sur STM32CubeIDE et à définir les pins à utiliser pour connecter la carte à l'écran. Etant donné que l'écran lcd utilise la technologie SPI, nous avons pu configuré la carte STM32 nucleo L07RZ pour que les broches d'entrée soient correctement configurées. 

La technologie SPI est normalement "full duplex", c’est-à-dire elle fonctionne dans les deux sens
(maître→esclave & esclave→maître). Dans le contexte de l’écran, est nécessaire uniquement la
communication maître→esclave. Les signaux du SPI sont les suivants :
• SCLK ⇒l’horloge de la communication
• MOSI (Master Output Slave Input) ⇒ le signal sur-lequel circule les données venant du⇒
maître
• MISO (Master Input Slave Output)⇒le signal surlequel cirucle les données venant de
l’esclave (pas utilisé comme expliqué précédemment)
• SS (Slave Select)⇒ permet de sélectionner l’esclave concerné par la transmission (pas
utilisé puisque le seul esclave est le drive de l’écran)

Ensuite, un code en C à été généré automatiquement à partir de ces informations. 
Une grosse partie de la séance a été dédié à la lecture et à la compréhension du code généré. 
La fin de la séance a été consacré à la recherche de documentation pour pouvoir commencer le codage de la transition de la carte Arduino à la carte STM32 nucleo pour l'affichage. 

##Séance du 14 mars 

###Gestion de l'écran avec une carte STM32

Nous avons continuer la compréhension du code de la génération automatique de STM32CubeIDE. 
Ensuite, nous avions pour objectif d'adapter notre code arduino à STM32. C'est à dire de nous servir de nos fonctions déjà utilisées pour arduino. 
Au fil de la séance, nous avons réfléchis et testé comment comment adapter l'un à l'autre. 


##Séance du 20 mars 

### Difficultés à l'adaptation du code 

Durant cette séance, nous avons encore essayé d'adapter notre code arduino avec la carte arduino à l'IDE STM32Cube IDE avec la carte STM32 nucleo L07RZ. 
Cependant, à la fin de la séance, nos testes ayant échoués, nous avons pris la décision de ne plus essayer d'adapter notre code mais de nous documenter davantage et d'en produire un nouveau. 


##Séance du 21 mars

### Affichage de l'écran avec SMT32CubeIDE

Nous avons donc repris nos rechecrhes afon de bien comprendre le fonctionnement
 
**Fonctionnement du driver PCD8544** 
L’écran fonctionne avec le driver PCD8544, ce qui permet de d’interpréter les commandes SPI et
d’agir en conséquence. C’est notamment ce qui permet de gérer l’affichage. Il y a deux modes en fonction de la pin DC
* **DC=0: Envoi de commandes**
La procédure dans le code C qui permet la bonne configuration du driver est la commande suivante : « LCD_PCD8544_init ». Elle va configurer les paramètres suivants :

• VOP pour « Operating voltage » : (valeur envoyée = 0x7F)
Intervient sur la tension appliqué au LCD, une formule est disponible pour calculer la valeur
optimale mais bon.
• BIAS : (valeur envoyée = 0x3)
C’est valeur est celle pour la tension de polarisation des pixels. Une formule est également
disponible pour choisir la meilleure valeur.
• Temperature control : (valeur envoyée = 0x3)
Permet de faire varier la tension du appliquée au LCD en fonction de la température, étant donné
que vous êtes dans des conditions qu’on pourrait qualifier d’optimales.


**Ecriture d'une donnée**

Le driver embarque une RAM de 48*84 bits (c’est la taille de l’écran, étonnant ?:O).
Pour être plus précis elle fait 6 banks de 8*84 bits.
Deux commandes permettent de sélectionner la position mémoire dans laquelle nous souhaitons
écrire. X étant la sélection de la colonne et Y la sélection de la memory bank.

Exemple: mettre le pixel (12,12) en noir
il faut alors écrire dans la
Bank 1, le bit 3 dans la colonne 12.
On a alors X = 11, Y = 1, et comme octet à écrire 0x08.
La procédure serait alors :
1. DC = 0, 0x41 (pour Y = 1)
2. DC = 0, 0x8B (pour X = 11)
3. DC = 1, 0x08 (pour avoir le bit 3 à 1)

**Ecriture d'un caratère ASCII**

Le fichier font6x8.h contient un tableau d’octets. Les octets sont par 6, pour faire un caractère.
Par exemple, si on veut écrire un « A » (majuscule) soit 65 en décimal. Donc dans le tableau, les
octets de 390 à 395, dans le fichier ils sont écrits 6 par lignes donc la 65ème ligne.
Nous avons donc les octets suivants : 0x7C, 0x12, 0x11, 0x12, 0x7C, 0x00.

Pour afficher une ligne de caractères, la fonction est la suivante :
« LCD_PCD8544_write_line(&gLcdScreen, 0, "Aquaponie") ; », Il va envoyer
successivement les octets qui codent chaque caractère. Comme l’écran ne fait que 84 pixels de
largeur, on ne peut envoyer que 84/6 (largeur d’un caractère) caractères soit 14.
Il va donc envoyer 6*14=84 octets via le SPI pour écrire une ligne pleine

**Librairie utilisée**
Les librairies permettent d’avoir les fonctions pour initialiser le driver, écrire dans la RAM du driver, etc. via le SPI déjà codée. Elles utilisent les lib HAL de CubeIDE. Les fonctions *_ll_* sont les fonctions les plus « élémentaires », elles font actions basiques comme envoyer une commande, envoyer un octet.

##Séance du 27 mars 

### Affichage de cractère sur l'ecran Lcd

Durant cette séance, nous avons appliqué les connaissances acquises durant la séance précédente pour afficher des caractères à l'écran. 
En mettant en place les différentes explications faites plus haut et après plusieurs tests, nous sommes arrivées à obtenir un affichage à l'écran. 

Nous avions effectué cette gestion de l'affichage dans un dossier séparé du reste du projet. L'objectif était maintenant de l'inclure dans le projet "watermonitoring" déjà existant. 


##Séance de 28 mars

Cette séance a principalement été consacrée à la réalisation du rapport final ainsi qu'a la préparation de la soutenance. 

##Séance du 30 mars 

Nous avons rencontré les IESE3 qui travaillent sur la carte de puissance de la serre pour échanger sur nos avancements respectifs.

























=======
Pour cela, nous avons commencé par créer un nouveau projet sur STM32CubeIDE et définir les pins à utiliser pour connecter la carte à l'écran. Étant donné que l'écran LCD utilise la technologie SPI, nous avons pu configurer la carte pour que les broches d'entrée soient correctement configurées. Ensuite, un code en C à été généré automatiquement à partir de ces informations. Une grosse partie de la séance a été dédiée à la lecture et à la compréhension du code généré. La fin de la séance a été consacrée à la recherche de documentation pour pouvoir commencer le codage de la transition de la carte Arduino à la carte STM32 nucleo pour l'affichage. 

## Séance du 27 mars

### L'écran LCD, de Arduino à STM32

Les branchements et le portage du code de l'écran dans le code de la carte STM32 du boîtier de la serre est un succès : nous sommes capables d'afficher les 4 données d'environnement de la serre sur l'écran en temps réel.
>>>>>>> 1e7ba5f6b6a9ac933faf4097dff5f75cd422c56b
